/* Pizzas */
INSERT INTO pizzas (id, nombre, descripcion) VALUES(1, 'Boloñesa', 'Mediana con salsa boloñesa');
INSERT INTO pizzas (id, nombre, descripcion) VALUES(2, 'Margarita', 'Mediana sin nada solo tomate');
INSERT INTO pizzas (id, nombre, descripcion) VALUES(3, 'Hawai', 'Pizza Piña');
INSERT INTO pizzas (id, nombre, descripcion) VALUES(4, 'Carbonara', 'Mediana con Salsa carbonara');
INSERT INTO pizzas (id, nombre, descripcion) VALUES(5, 'Vegetal', 'Sin carne');
INSERT INTO pizzas (id, nombre, descripcion) VALUES(6, 'Jamón y queso', 'Jamón y queso');
INSERT INTO pizzas (id, nombre, descripcion) VALUES(7, 'Atún', 'Mediana de atún');
INSERT INTO pizzas (id, nombre, descripcion) VALUES(8, 'Picante', 'Mediana muy picante');

/* Pastas */
INSERT INTO pastas (id, nombre, descripcion) VALUES(1, 'Espaguetis', 'Con salsa boloñesa');
INSERT INTO pastas (id, nombre, descripcion) VALUES(2, 'Macarrones', 'Con salsa boloñesa');
INSERT INTO pastas (id, nombre, descripcion) VALUES(3, 'Canelones', 'Con salsa carbonara');
INSERT INTO pastas (id, nombre, descripcion) VALUES(4, 'Lasaña', 'Tomate y espinacas');
INSERT INTO pastas (id, nombre, descripcion) VALUES(5, 'Espaguetis', 'Con salsa carbonara');
INSERT INTO pastas (id, nombre, descripcion) VALUES(6, 'Ravioli', 'Salsa ravioli');

/* Bebidas */
INSERT INTO bebidas (id, nombre, cantidad_ml) VALUES(1, 'Coca cola', 330);
INSERT INTO bebidas (id, nombre, cantidad_ml) VALUES(2, 'Pepsi', 330);
INSERT INTO bebidas (id, nombre, cantidad_ml) VALUES(3, 'Agua', 500);
INSERT INTO bebidas (id, nombre, cantidad_ml) VALUES(4, 'Cerveza', 330);
INSERT INTO bebidas (id, nombre, cantidad_ml) VALUES(5, 'Nestea', 330);
INSERT INTO bebidas (id, nombre, cantidad_ml) VALUES(6, 'Cerveza grande', 2000);
INSERT INTO bebidas (id, nombre, cantidad_ml) VALUES(7, 'Agua grande', 1500);
INSERT INTO bebidas (id, nombre, cantidad_ml) VALUES(8, 'Sidra', 300);

/* Ingredientes*/
INSERT INTO ingredientes (id, nombre) VALUES(1, 'Tomate');
INSERT INTO ingredientes (id, nombre) VALUES(2, 'Queso');
INSERT INTO ingredientes (id, nombre) VALUES(3, 'Jamón');
INSERT INTO ingredientes (id, nombre) VALUES(4, 'Pimiento');
INSERT INTO ingredientes (id, nombre) VALUES(5, 'Cebolla');
INSERT INTO ingredientes (id, nombre) VALUES(6, 'Nata');
INSERT INTO ingredientes (id, nombre) VALUES(7, 'Piña');
INSERT INTO ingredientes (id, nombre) VALUES(8, 'Jalapeño');
INSERT INTO ingredientes (id, nombre) VALUES(9, 'Ravioli');
INSERT INTO ingredientes (id, nombre) VALUES(10, 'Espinacas');
INSERT INTO ingredientes (id, nombre) VALUES(11, 'Espaguetis');
INSERT INTO ingredientes (id, nombre) VALUES(12, 'Carne');
INSERT INTO ingredientes (id, nombre) VALUES(13, 'Atún');

/* Establecimientos */
INSERT INTO establecimientos (id, direccion, localidad, codigo_postal, provincia) VALUES(1, 'Av. Mediterraneo, 8', 'Teulada', '03725', 'Alicante');
INSERT INTO establecimientos (id, direccion, localidad, codigo_postal, provincia) VALUES(2, 'Av. Augusta, 9', 'Javea', '03730', 'Alicante');
INSERT INTO establecimientos (id, direccion, localidad, codigo_postal, provincia) VALUES(3, 'Av. San Sebastian, 10', 'Alicante', '03002', 'Alicante');
INSERT INTO establecimientos (id, direccion, localidad, codigo_postal, provincia) VALUES(4, 'Av. Del Sol, 11', 'Gandia', '46701', 'Valencia');

/* Pizzas ingredientes (relacion) */
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(1,1);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(1,2);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(1,12);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(1,5);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(2,1);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(2,2);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(2,12);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(2,5);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(3,1);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(3,2);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(3,3);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(3,7);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(4,5);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(4,6);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(4,2);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(5,1);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(5,2);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(5,4);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(5,5);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(5,10);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(6,1);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(6,2);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(6,3);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(7,1);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(7,2);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(7,5);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(7,13);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(8,1);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(8,2);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(8,4);
INSERT INTO pizza_ingrediente (pizza_id, ingrediente_id) VALUES(8,8);