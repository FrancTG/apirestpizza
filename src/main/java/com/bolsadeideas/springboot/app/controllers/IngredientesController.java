package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bolsadeideas.springboot.app.models.entity.Ingrediente;
import com.bolsadeideas.springboot.app.models.service.IIngredienteService;
import com.bolsadeideas.springboot.app.util.paginador.PageRenderer;

import net.bytebuddy.description.field.FieldDescription.InGenericShape;

@Controller
@SessionAttributes("ingrediente")
@RequestMapping("/ingrediente")
public class IngredientesController {

	@Autowired
	private IIngredienteService ingredienteService;
	
	/**
	 * Lista todos los ingredientes en el HTML listarIngredientes
	 * @param page numero de paginas
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/listar")
	public String listarIngrediente(@RequestParam(name = "page",defaultValue = "0") int page, Model model) {
		Pageable pageRequest = PageRequest.of(page, 5); // Paginas
		
		Page<Ingrediente> ingredientes = ingredienteService.findAll(pageRequest); // Lista de ingredientes por pagina
		PageRenderer<Ingrediente> pageRenderer = new PageRenderer<>("/ingrediente/listar", ingredientes);
		model.addAttribute("titulo", "Listado de ingredientes");
		model.addAttribute("ingredientes", ingredientes);
		model.addAttribute("totalingredientes",ingredienteService.findAll());
		model.addAttribute("page",pageRenderer);
		return "listarIngrediente";
	}
	
	@RequestMapping(value = "/form")
	public String crear(Map<String, Object> model) {

		Ingrediente ingrediente = new Ingrediente();
		model.put("ingrediente", ingrediente);
		model.put("titulo", "Formulario de Ingrediente");
		return "formIngrediente";
	}
	
	@RequestMapping(value="/form/{id}")
	public String editar(@PathVariable(value="id") Long id, Map<String, Object> model) {
		
		Ingrediente ingrediente = null;
		
		if(id > 0) {
			ingrediente = ingredienteService.findOne(id);
		} else {
			return "redirect:listar";
		}
		model.put("ingrediente", ingrediente);
		model.put("titulo", "Editar Ingrediente");
		return "formIngrediente";
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String guardar(@Valid Ingrediente ingrediente, BindingResult result, Model model, SessionStatus status) {
		if(result.hasErrors()) {
			model.addAttribute("titulo", "Formulario de Ingrediente");
			return "formIngrediente";
		}
		
		ingredienteService.save(ingrediente);
		status.setComplete();
		return "redirect:listar";
	}
	
	@RequestMapping(value="/eliminar/{id}")
	public String eliminar(@PathVariable(value="id") Long id) {
		
		if(id > 0) {
			ingredienteService.delete(id);
		}
		return "redirect:/ingrediente/listar";
	}
}
