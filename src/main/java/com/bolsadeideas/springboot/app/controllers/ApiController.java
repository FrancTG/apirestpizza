package com.bolsadeideas.springboot.app.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bolsadeideas.springboot.app.models.entity.Bebida;
import com.bolsadeideas.springboot.app.models.entity.Establecimiento;
import com.bolsadeideas.springboot.app.models.entity.Ingrediente;
import com.bolsadeideas.springboot.app.models.entity.Pasta;
import com.bolsadeideas.springboot.app.models.entity.Pizza;
import com.bolsadeideas.springboot.app.models.service.IBebidasService;
import com.bolsadeideas.springboot.app.models.service.IEstablecimientoService;
import com.bolsadeideas.springboot.app.models.service.IIngredienteService;
import com.bolsadeideas.springboot.app.models.service.IPastaService;
import com.bolsadeideas.springboot.app.models.service.IPizzaService;

@Controller
@RequestMapping(value = "/api")
public class ApiController {

	@Autowired
	private IPastaService pastaService;
	
	@Autowired
	private IPizzaService pizzaService;
	
	@Autowired
	private IEstablecimientoService establecimientoService;
	
	@Autowired
	private IIngredienteService ingredienteService;
	
	@Autowired
	private IBebidasService bebidaService;
	
	
	@GetMapping(value = "/pasta")
	public ResponseEntity<Collection<Pasta>> apiPasta() {
	    return ResponseEntity.ok(pastaService.findAll());
	}
	
	@GetMapping(value = "/pizza")
	public ResponseEntity<Collection<Pizza>> apiPizza() {
	    return ResponseEntity.ok(pizzaService.findAll());
	}
	
	@GetMapping(value = "/establecimiento")
	public ResponseEntity<Collection<Establecimiento>> apiEstablecimiento() {
	    return ResponseEntity.ok(establecimientoService.findAll());
	}
	
	@GetMapping(value = "/ingrediente")
	public ResponseEntity<Collection<Ingrediente>> apiIngrediente() {
	    return ResponseEntity.ok(ingredienteService.findAll());
	}
	
	@GetMapping(value = "/bebida")
	public ResponseEntity<Collection<Bebida>> apiBebida() {
	    return ResponseEntity.ok(bebidaService.findAll());
	}
}
