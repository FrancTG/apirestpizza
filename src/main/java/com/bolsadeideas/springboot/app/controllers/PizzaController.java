package com.bolsadeideas.springboot.app.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Ingrediente;
import com.bolsadeideas.springboot.app.models.entity.Pizza;
import com.bolsadeideas.springboot.app.models.service.IIngredienteService;
import com.bolsadeideas.springboot.app.models.service.IPizzaService;
import com.bolsadeideas.springboot.app.util.paginador.PageRenderer;

@Controller
@SessionAttributes("pizza")
@RequestMapping("/pizza")
public class PizzaController {
	
	@Autowired
	private IPizzaService pizzaService;
	
	@Autowired
	private IIngredienteService ingredienteService; //Muestra la lista de ingredientes disponibles a la hora de editar o crear

	
	@RequestMapping(value = "/listar")
	public String listarPizza(@RequestParam(name="page",defaultValue = "0") int page, Model model) {
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page<Pizza> pizzas = pizzaService.findAll(pageRequest);
		PageRenderer<Pizza> pageRenderer = new PageRenderer<>("/pizza/listar", pizzas);
		
		model.addAttribute("titulo", "Listado de pizzas");
		model.addAttribute("pizza",pizzaService.findOne(1L));
		model.addAttribute("pizzas", pizzas);
		model.addAttribute("totalpizzas",pizzaService.findAll());
		model.addAttribute("page",pageRenderer);
		return "listarPizza";
	}
	
	@RequestMapping(value = "/form")
	public String crear(Map<String, Object> model) {
		
		Pizza pizza = new Pizza();
		model.put("pizza", pizza);
		model.put("ingredientes", ingredienteService.findAll());
		model.put("titulo", "Formulario de Pizza");
		
		return "formPizza";
	}
	
	@RequestMapping(value="/form/{id}")
	public String editar(@PathVariable(value="id") Long id, Map<String, Object> model) {
		
		Pizza pizza = null;
		
		if(id > 0) {
			pizza = pizzaService.findOne(id);
		} else {
			return "redirect:listar";
		}
		model.put("pizza", pizza);
		model.put("ingredientes", ingredienteService.findAll());
		model.put("titulo", "Editar Pizza");
		return "formPizza";
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String guardar(@Valid Pizza pizza, BindingResult result, Model model, SessionStatus status) {
		if(result.hasErrors()) {
			model.addAttribute("titulo", "Formulario de Pizza");
			return "formPizza";
		}
		
		pizzaService.save(pizza);
		status.setComplete();
		return "redirect:listar";
	}
	
	@RequestMapping(value="/eliminar/{id}")
	public String eliminar(@PathVariable(value="id") Long id) {
		
		if(id > 0) {
			pizzaService.delete(id);
		}
		return "redirect:/pizza/listar";
	}
}
