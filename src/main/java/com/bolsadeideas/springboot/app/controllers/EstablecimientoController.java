package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bolsadeideas.springboot.app.models.entity.Establecimiento;
import com.bolsadeideas.springboot.app.models.service.IEstablecimientoService;
import com.bolsadeideas.springboot.app.util.paginador.PageRenderer;

@Controller
@SessionAttributes("establecimiento")
@RequestMapping("/establecimiento")
public class EstablecimientoController {
	
	@Autowired
	private IEstablecimientoService establecimientoService;
	
	/**
	 * Lista todos los establecimientos en el html listarEstablecimientos.html
	 * @param page numero de pagina
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/listar")
	public String listarEstablecimiento(@RequestParam(name = "page",defaultValue = "0") int page,Model model) {
		Pageable pageRequest = PageRequest.of(page,5); // Paginas
		
		Page<Establecimiento> establecimientos = establecimientoService.findAll(pageRequest); // Lista de establecimientos por pagina
		PageRenderer<Establecimiento> pageRenderer = new PageRenderer<>("/establecimiento/listar",establecimientos);
		model.addAttribute("titulo", "Listado de establecimientos");
		model.addAttribute("establecimientos", establecimientos);
		model.addAttribute("totalestablecimientos",establecimientoService.findAll());
		model.addAttribute("page",pageRenderer);
		return "listarEstablecimiento";
	}
	
	@RequestMapping(value = "/form")
	public String crear(Map<String, Object> model) {

		Establecimiento establecimiento = new Establecimiento();
		model.put("establecimiento", establecimiento);
		model.put("titulo", "Formulario de Establecimiento");
		return "formEstablecimiento";
	}
	
	@RequestMapping(value="/form/{id}")
	public String editar(@PathVariable(value="id") Long id, Map<String, Object> model) {
		
		Establecimiento establecimiento = null;
		
		if(id > 0) {
			establecimiento = establecimientoService.findOne(id);
		} else {
			return "redirect:listar";
		}
		model.put("establecimiento", establecimiento);
		model.put("titulo", "Editar Establecimiento");
		return "formEstablecimiento";
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String guardar(@Valid Establecimiento establecimiento, BindingResult result, Model model, SessionStatus status) {
		if(result.hasErrors()) {
			model.addAttribute("titulo", "Formulario de Establecimiento");
			return "formEstablecimiento";
		}
		
		establecimientoService.save(establecimiento);
		status.setComplete();
		return "redirect:listar";
	}
	
	@RequestMapping(value="/eliminar/{id}")
	public String eliminar(@PathVariable(value="id") Long id) {
		
		if(id > 0) {
			establecimientoService.delete(id);
		}
		return "redirect:/establecimiento/listar";
	}
	
}
