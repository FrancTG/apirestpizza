package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bolsadeideas.springboot.app.models.entity.Bebida;
import com.bolsadeideas.springboot.app.models.service.IBebidasService;
import com.bolsadeideas.springboot.app.util.paginador.PageRenderer;

@Controller
@SessionAttributes("bebida")
@RequestMapping("/bebida")
public class BebidaController {
	
	@Autowired
	private IBebidasService bebidasService;

	/**
	 * Lista todas las bebidas en el html listarBebidas
	 * @param page numero pagina
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/listar")
	public String listarBebida(@RequestParam(name = "page",defaultValue = "0")int page,Model model) {
		Pageable pageRequest = PageRequest.of(page,5); // Numero de paginas 
		
		Page<Bebida> bebidas = bebidasService.findAll(pageRequest); // Lista de bebidas por pagina
		PageRenderer<Bebida> pageRenderer = new PageRenderer<>("/bebida/listar",bebidas); 
		model.addAttribute("titulo", "Listado de bebidas");
		model.addAttribute("totalbebidas",bebidasService.findAll());
		model.addAttribute("bebidas", bebidas);
		model.addAttribute("page",pageRenderer);
		return "listarBebida";
	}
	
	@RequestMapping(value = "/form")
	public String crear(Map<String, Object> model) {

		Bebida bebida = new Bebida();
		model.put("bebida", bebida);
		model.put("titulo", "Formulario de Bebida");
		return "formBebida";
	}
	
	@RequestMapping(value="/form/{id}")
	public String editar(@PathVariable(value="id") Long id, Map<String, Object> model) {
		
		Bebida bebida = null;
		
		if(id > 0) {
			bebida = bebidasService.findOne(id);
		} else {
			return "redirect:listar";
		}
		model.put("bebida", bebida);
		model.put("titulo", "Editar Bebida");
		return "formBebida";
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String guardar(@Valid Bebida bebida, BindingResult result, Model model, SessionStatus status) {
		if(result.hasErrors()) {
			model.addAttribute("titulo", "Formulario de Bebida");
			return "formBebida";
		}
		
		bebidasService.save(bebida);
		status.setComplete();
		return "redirect:listar";
	}
	
	@RequestMapping(value="/eliminar/{id}")
	public String eliminar(@PathVariable(value="id") Long id) {
		
		if(id > 0) {
			bebidasService.delete(id);
		}
		return "redirect:/bebida/listar";
	}
	
}
