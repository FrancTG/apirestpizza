package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.TinyBitSet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.bolsadeideas.springboot.app.models.entity.Pasta;
import com.bolsadeideas.springboot.app.models.service.IPastaService;
import com.bolsadeideas.springboot.app.util.paginador.PageRenderer;

@Controller
@SessionAttributes("pasta")
@RequestMapping("/pasta")
public class PastaController {
	
	@Autowired
	private IPastaService pastaService;
	
	/**
	 * Lista todas las pastas en un html 
	 * @param page
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/listar")
	public String listarPasta(@RequestParam(name = "page",defaultValue = "0") int page, Model model) {
		Pageable pageRequest = PageRequest.of(page,5); //Paginas
		
		Page<Pasta> pastas = pastaService.findAll(pageRequest); //Lista de pastaas por pagina
		PageRenderer<Pasta> pageRenderer = new PageRenderer<>("/pasta/listar",pastas);
		model.addAttribute("titulo", "Listado de pastas");
		model.addAttribute("pastas", pastas);
		model.addAttribute("totalpasta",pastaService.findAll());
		model.addAttribute("page",pageRenderer);
		return "listarPasta";
	}
	
	@RequestMapping(value = "/form")
	public String crear(Map<String, Object> model) {

		Pasta pasta = new Pasta();
		model.put("pasta", pasta);
		model.put("titulo", "Formulario de Pasta");
		return "formPasta";
	}
	
	@RequestMapping(value="/form/{id}")
	public String editar(@PathVariable(value="id") Long id, Map<String, Object> model) {
		
		Pasta pasta = null;
		
		if(id > 0) {
			pasta = pastaService.findOne(id);
		} else {
			return "redirect:listar";
		}
		model.put("pasta", pasta);
		model.put("titulo", "Editar Pasta");
		return "formPasta";
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String guardar(@Valid Pasta pasta, BindingResult result, Model model, SessionStatus status) {
		if(result.hasErrors()) {
			model.addAttribute("titulo", "Formulario de Pasta");
			return "formPasta";
		}
		
		pastaService.save(pasta);
		status.setComplete();
		return "redirect:listar";
	}
	
	@RequestMapping(value="/eliminar/{id}")
	public String eliminar(@PathVariable(value="id") Long id) {
		
		if(id > 0) {
			pastaService.delete(id);
		}
		return "redirect:/pasta/listar";
	}

}
