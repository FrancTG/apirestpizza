package com.bolsadeideas.springboot.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

	/**
	 * Pagina principal con carousel
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"/home","/"})
	public String mostrarMainMenu(Model model) {
		model.addAttribute("titulo","Pagina principal");
		return "home";
	}
}
