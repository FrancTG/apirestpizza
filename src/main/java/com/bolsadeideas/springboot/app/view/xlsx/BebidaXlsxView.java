package com.bolsadeideas.springboot.app.view.xlsx;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.bolsadeideas.springboot.app.models.entity.Bebida;

@Component("listarBebida")
public class BebidaXlsxView extends AbstractXlsView{

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		List<Bebida> listaBebidas = (List<Bebida>) model.get("totalbebidas");
		
		Sheet sheet = workbook.createSheet("BebidaSpring");
		
		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue("Datos bebida");
		
		row = sheet.createRow(1);
		row.createCell(0).setCellValue("Nombre");
		row.createCell(1).setCellValue("Cantidad_ml");
		
		int contador = 1;
		for (Bebida bebida : listaBebidas) {
			contador++;
			row = sheet.createRow(contador);
			row.createCell(0).setCellValue(bebida.getNombre());
			row.createCell(1).setCellValue(bebida.getCantidad_ml()+"");
		}
	}
	
}
