package com.bolsadeideas.springboot.app.view.pdf;

import java.awt.Color;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.bolsadeideas.springboot.app.models.entity.Ingrediente;
import com.bolsadeideas.springboot.app.models.entity.Pizza;
import com.lowagie.text.Document;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@Component("listarPizza.pdf")
public class PizzaPdfView extends AbstractPdfView{

	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		
		List<Pizza> listaPizzas = (List<Pizza>) model.get("totalpizzas");
		
		PdfPTable tabla = new PdfPTable(3);
		
		PdfPCell cell = null;
		cell = new PdfPCell(new Phrase("Datos de pizza"));
		cell.setBackgroundColor(new Color(184,218,255));
		cell.setHorizontalAlignment(1);
		cell.setColspan(3);
		cell.setPadding(7f);
		tabla.addCell(cell);
		
		tabla.addCell("Nombre");
		tabla.addCell("Descripción");
		tabla.addCell("Ingredientes");
		
		for (Pizza pizza : listaPizzas) {
			tabla.addCell(pizza.getNombre());
			tabla.addCell(pizza.getDescripcion());
			String ingrediString = "";
			for (Ingrediente ing:pizza.getIngredientes()) {
				ingrediString = ingrediString + ing.getNombre()+ ", ";
			}
			
			tabla.addCell(ingrediString);			
		}
		document.add(tabla);
		
	}
}
