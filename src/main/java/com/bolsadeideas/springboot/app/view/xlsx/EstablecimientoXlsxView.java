package com.bolsadeideas.springboot.app.view.xlsx;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.bolsadeideas.springboot.app.models.entity.Establecimiento;
import com.bolsadeideas.springboot.app.models.entity.Ingrediente;
import com.bolsadeideas.springboot.app.models.entity.Pizza;

@Component("listarEstablecimiento")
public class EstablecimientoXlsxView extends AbstractXlsView{

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		List<Establecimiento> listaEstablecimientos = (List<Establecimiento>) model.get("totalestablecimientos");
		
		Sheet sheet = workbook.createSheet("EstablecimientosSpring");
		
		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue("Datos establecimientos");
		
		row = sheet.createRow(1);
		row.createCell(0).setCellValue("Direccion");
		row.createCell(1).setCellValue("Localidad");
		row.createCell(2).setCellValue("Codigo Postal");
		row.createCell(3).setCellValue("Provincia");
		
		int contador = 1;
		for (Establecimiento establecimiento : listaEstablecimientos) {
			contador++;
			row = sheet.createRow(contador);
			row.createCell(0).setCellValue(establecimiento.getDireccion());
			row.createCell(1).setCellValue(establecimiento.getLocalidad());
			row.createCell(2).setCellValue(establecimiento.getCodigo_postal());
			row.createCell(3).setCellValue(establecimiento.getProvincia());
			
		}
	}

}
