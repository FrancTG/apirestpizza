package com.bolsadeideas.springboot.app.view.pdf;

import java.awt.Color;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.bolsadeideas.springboot.app.models.entity.Establecimiento;
import com.bolsadeideas.springboot.app.models.entity.Ingrediente;
import com.bolsadeideas.springboot.app.models.entity.Pizza;
import com.lowagie.text.Document;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@Component("listarEstablecimiento.pdf")
public class EstablecimientoPdfView extends AbstractPdfView{

	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		List<Establecimiento> listaEstablecimientos = (List<Establecimiento>) model.get("totalestablecimientos");
		
		PdfPTable tabla = new PdfPTable(4);
		
		PdfPCell cell = null;
		cell = new PdfPCell(new Phrase("Datos de establecimiento"));
		cell.setBackgroundColor(new Color(184,218,255));
		cell.setHorizontalAlignment(1);
		cell.setColspan(4);
		cell.setPadding(7f);
		tabla.addCell(cell);
		
		tabla.addCell("Dirección");
		tabla.addCell("Localidad");
		tabla.addCell("Codigo Postal");
		tabla.addCell("Provincia");
		
		for (Establecimiento establecimiento : listaEstablecimientos) {
			tabla.addCell(establecimiento.getDireccion());
			tabla.addCell(establecimiento.getLocalidad());
			tabla.addCell(establecimiento.getCodigo_postal());
			tabla.addCell(establecimiento.getProvincia());			
		}
		document.add(tabla);
		
	}

}
