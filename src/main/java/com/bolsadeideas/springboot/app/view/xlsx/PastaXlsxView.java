package com.bolsadeideas.springboot.app.view.xlsx;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.bolsadeideas.springboot.app.models.entity.Ingrediente;
import com.bolsadeideas.springboot.app.models.entity.Pasta;
import com.bolsadeideas.springboot.app.models.entity.Pizza;

@Component("listarPasta")
public class PastaXlsxView extends AbstractXlsView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		List<Pasta> listaPasta = (List<Pasta>) model.get("totalpasta");
		
		Sheet sheet = workbook.createSheet("PastaSpring");
		
		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue("Datos pasta");
		
		row = sheet.createRow(1);
		row.createCell(0).setCellValue("Nombre");
		row.createCell(1).setCellValue("Descripción");
		
		int contador = 1;
		for (Pasta pasta : listaPasta) {
			contador++;
			row = sheet.createRow(contador);
			row.createCell(0).setCellValue(pasta.getNombre());
			row.createCell(1).setCellValue(pasta.getDescripcion());
		}
	}

}
