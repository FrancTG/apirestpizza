package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IBebidaDao;
import com.bolsadeideas.springboot.app.models.entity.Bebida;

@Service
public class BebidaServiceImpl implements IBebidasService{
	
	@Autowired
	private IBebidaDao bebidaDao;

	@Override
	@Transactional(readOnly = true)
	public List<Bebida> findAll() {
		// TODO Auto-generated method stub
		return (List<Bebida>) bebidaDao.findAll();
	}

	@Override
	public void save(Bebida bebida) {
		// TODO Auto-generated method stub
		bebidaDao.save(bebida);
	}

	@Override
	@Transactional(readOnly = true)
	public Bebida findOne(Long id) {
		// TODO Auto-generated method stub
		return bebidaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		bebidaDao.deleteById(id);
		
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Bebida> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return bebidaDao.findAll(pageable);
	}

}
