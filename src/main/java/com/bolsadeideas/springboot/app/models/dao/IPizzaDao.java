package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.bolsadeideas.springboot.app.models.entity.Pizza;

public interface IPizzaDao extends PagingAndSortingRepository<Pizza, Long>{

}
