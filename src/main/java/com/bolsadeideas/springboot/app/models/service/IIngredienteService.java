package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.bolsadeideas.springboot.app.models.entity.Bebida;
import com.bolsadeideas.springboot.app.models.entity.Ingrediente;
import com.bolsadeideas.springboot.app.models.entity.Pizza;

public interface IIngredienteService{
	
	public List<Ingrediente> findAll();
	
	public Page<Ingrediente> findAll(Pageable pageable);
	
	public void save(Ingrediente ingrediente);
	
	public Ingrediente findOne(Long id);
	
	public void delete(Long id);

}
