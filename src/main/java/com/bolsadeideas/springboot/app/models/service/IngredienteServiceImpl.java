package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IIngredienteDao;
import com.bolsadeideas.springboot.app.models.entity.Ingrediente;

@Service
public class IngredienteServiceImpl implements IIngredienteService{

	@Autowired
	private IIngredienteDao ingredienteDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Ingrediente> findAll() {
		// TODO Auto-generated method stub
		return (List<Ingrediente>) ingredienteDao.findAll();
	}

	@Override
	@Transactional
	public void save(Ingrediente ingrediente) {
		// TODO Auto-generated method stub
		ingredienteDao.save(ingrediente);
	}

	@Override
	@Transactional(readOnly = true)
	public Ingrediente findOne(Long id) {
		// TODO Auto-generated method stub
		return ingredienteDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		ingredienteDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Ingrediente> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return ingredienteDao.findAll(pageable);
	}
	


}
