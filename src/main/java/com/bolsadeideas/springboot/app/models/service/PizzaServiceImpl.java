package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IPizzaDao;
import com.bolsadeideas.springboot.app.models.entity.Pizza;

@Service
public class PizzaServiceImpl implements IPizzaService{

	@Autowired
	private IPizzaDao pizzaDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Pizza> findAll() {
		// TODO Auto-generated method stub
		return (List<Pizza>) pizzaDao.findAll();
	}

	@Override
	@Transactional
	public void save(Pizza pizza) {
		// TODO Auto-generated method stub
		pizzaDao.save(pizza);
	}

	@Override
	@Transactional(readOnly = true)
	public Pizza findOne(Long id) {
		// TODO Auto-generated method stub
		return pizzaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		pizzaDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Pizza> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return pizzaDao.findAll(pageable);
	}

	

}
