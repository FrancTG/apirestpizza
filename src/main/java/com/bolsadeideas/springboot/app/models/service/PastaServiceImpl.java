package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IPastaDao;
import com.bolsadeideas.springboot.app.models.entity.Pasta;

@Service
public class PastaServiceImpl implements IPastaService{
	
	@Autowired
	private IPastaDao pastaDao;

	@Override
	@Transactional(readOnly = true)
	public List<Pasta> findAll() {
		// TODO Auto-generated method stub
		return (List<Pasta>) pastaDao.findAll();
	}

	@Override
	public void save(Pasta pasta) {
		// TODO Auto-generated method stub
		pastaDao.save(pasta);
	}

	@Override
	@Transactional(readOnly = true)
	public Pasta findOne(Long id) {
		// TODO Auto-generated method stub
		return pastaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		pastaDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Pasta> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return pastaDao.findAll(pageable);
	}

}
