package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.bolsadeideas.springboot.app.models.entity.Establecimiento;

public interface IEstablecimientoDao extends PagingAndSortingRepository<Establecimiento, Long>{

}
