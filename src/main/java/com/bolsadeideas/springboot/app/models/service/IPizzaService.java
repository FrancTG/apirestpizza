package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.bolsadeideas.springboot.app.models.entity.Pizza;

public interface IPizzaService {

	public List<Pizza> findAll();
	
	public Page<Pizza> findAll(Pageable pageable);

	public void save(Pizza pizza);
	
	public Pizza findOne(Long id);
	
	public void delete(Long id);
}
