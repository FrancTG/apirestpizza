package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.bolsadeideas.springboot.app.models.entity.Bebida;
import com.bolsadeideas.springboot.app.models.entity.Pizza;

public interface IBebidasService {

	public List<Bebida> findAll();
	
	public Page<Bebida> findAll(Pageable pageable);
	
	public void save(Bebida bebida);
	
	public Bebida findOne(Long id);
	
	public void delete(Long id);
}
