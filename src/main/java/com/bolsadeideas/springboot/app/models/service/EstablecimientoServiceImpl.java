package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IEstablecimientoDao;
import com.bolsadeideas.springboot.app.models.entity.Establecimiento;

@Service
public class EstablecimientoServiceImpl implements IEstablecimientoService{

	@Autowired
	private IEstablecimientoDao establecimientoDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Establecimiento> findAll() {
		// TODO Auto-generated method stub
		return (List<Establecimiento>) establecimientoDao.findAll();
	}

	@Override
	@Transactional
	public void save(Establecimiento establecimiento) {
		// TODO Auto-generated method stub
		establecimientoDao.save(establecimiento);
	}

	@Override
	@Transactional(readOnly = true)
	public Establecimiento findOne(Long id) {
		// TODO Auto-generated method stub
		return establecimientoDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		establecimientoDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Establecimiento> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return establecimientoDao.findAll(pageable);
	}

}
