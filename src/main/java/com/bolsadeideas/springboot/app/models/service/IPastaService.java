package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.bolsadeideas.springboot.app.models.entity.Pasta;
import com.bolsadeideas.springboot.app.models.entity.Pizza;

public interface IPastaService {
	
	public List<Pasta> findAll();
	
	public Page<Pasta> findAll(Pageable pageable);
	
	public void save(Pasta pasta);
	
	public Pasta findOne(Long id);
	
	public void delete(Long id);

}
