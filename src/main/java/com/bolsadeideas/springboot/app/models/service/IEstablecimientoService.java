package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.bolsadeideas.springboot.app.models.entity.Establecimiento;
import com.bolsadeideas.springboot.app.models.entity.Pizza;

public interface IEstablecimientoService {

	public List<Establecimiento> findAll();
	
	public Page<Establecimiento> findAll(Pageable pageable);
	
	public void save(Establecimiento establecimiento);
	
	public Establecimiento findOne(Long id);
	
	public void delete(Long id);
}
